from django import forms
from .models import SearchBar
from django.forms import ModelForm
from addwarung.models import Warung

class SearchForm(ModelForm):
    class Meta:
        model = SearchBar
        fields = ['namaProduk','lokasi']
        labels = {
            'namaProduk':'Nama Produk','lokasi':'Lokasi'
        }
        widgets = {
            'namaProduk': forms.TextInput(attrs={'class': 'form-control form-control-sm',
                                               'type': 'text',
                                               'placeholder': 'Nama produk, misal telur'}),
            'lokasi': forms.Select(attrs={'class': 'form-control form-control-sm'})
        }

