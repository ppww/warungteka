from django.shortcuts import render, redirect
from .forms import SearchForm
from .models import SearchBar
from addwarung.models import Barang, Warung
from django.db.models import FilteredRelation, Q


def search(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            form.save()
            searchObjects = SearchBar.objects.all()
            queryNama = SearchBar.objects.latest('created_at').namaProduk
            queryLokasi = SearchBar.objects.latest('created_at').lokasi
            barangFiltered = Barang.objects.filter(Q(name__icontains=queryNama) & Q(warung__address__icontains=queryLokasi))
            content = {'title': 'Search',
                       'searchObjects': searchObjects,
                       'barangFiltered': barangFiltered,
                       'queryNama':queryNama,
                       'queryLokasi': queryLokasi,
                       'form': form}
            return render(request, 'search.html', content)
    else:
        form = SearchForm

    searchObjects = SearchBar.objects.all()
    barangFiltered = Barang.objects.filter(Q(name__icontains='.0'))
    content = {'title': 'Search',
               'searchObjects': searchObjects,
               'barangFiltered': barangFiltered,
               'form': form}
    return render(request, 'search.html',content)
