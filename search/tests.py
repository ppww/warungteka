from django.test import TestCase, Client
from django.urls import resolve
from .views import search
from .models import SearchBar
from .forms import SearchForm
from django.db.models import Q
from addwarung.models import Barang, Warung

class SearchTekaTest(TestCase):
    #     GENERAL TESTS      #
    def test_url_search_is_exist(self):
        response = Client().get('/search/')
        self.assertEqual(response.status_code, 200)

    def test_using_search_func(self):
        found = resolve('/search/')
        self.assertEqual(found.func, search)

    def test_using_search_template(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response,'search.html')

    #     TEST FOR MODELS      #
    def test_model_can_create_new(self):
        new_search = SearchBar.objects.create(namaProduk='mie instan',lokasi='Jakarta')
        counting_all_available_search = SearchBar.objects.all().count()
        self.assertEqual(counting_all_available_search, 1)

    #     TEST FOR FORMS     #
    def test_search_post_success_and_render_the_result(self):
        barang = 'telur'
        tempat = 'Jakarta'
        response = Client().post('/search/', {'namaProduk': 'telur', 'lokasi': 'Jakarta'})
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn(barang, html_response)
        self.assertIn(tempat, html_response)

    def test_forms_valid(self):
        searchbar = SearchBar(namaProduk='telur',lokasi='Jakarta')
        form = SearchForm({'namaProduk': 'telur', 'lokasi': 'JAKARTA'},instance=searchbar)
        self.assertTrue(form.is_valid())

    def test_form_saves_values_to_instance_user_on_save(self):
        current = SearchBar.objects.all().count()
        searchbar = SearchBar(namaProduk='telur', lokasi='Jakarta')
        form = SearchForm({'namaProduk': 'telur', 'lokasi': 'JAKARTA'}, instance=searchbar)

        if (form.is_valid()):
            self.assertTrue(form.save())
            allObjects = SearchBar.objects.all().count()
            self.assertEqual(allObjects,current+1)

            barang = 'telur'
            tempat = 'JAKARTA'
            cekqueryBarang = SearchBar.objects.latest('created_at').namaProduk
            cekqueryLokasi = SearchBar.objects.latest('created_at').lokasi
            self.assertEqual(cekqueryBarang, barang)
            self.assertEqual(cekqueryLokasi, tempat)

            response = Client().get('/search/')
            html_response = response.content.decode('utf8')
            self.assertIn(cekqueryBarang, html_response)
            self.assertIn(cekqueryLokasi, html_response)