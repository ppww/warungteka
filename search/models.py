from django.db import models
from addwarung.models import Warung

class SearchBar(models.Model):
    TEMPAT_CHOICES = [
        ('JAKARTA', 'Jakarta'),
        ('BANDUNG','Bandung'),
        ('BOGOR','Bogor'),
    ]

    TEMPAT_CHOICES = [('',''),('JAKARTA','Jakarta'),]
    for warung in Warung.objects.all():
        namaLokasi = warung.address
        upperLokasi = namaLokasi.upper()
        if (upperLokasi,namaLokasi) not in TEMPAT_CHOICES:
            TEMPAT_CHOICES.append((upperLokasi,namaLokasi))

    namaProduk = models.CharField(max_length=100)
    lokasi = models.CharField(max_length=100, choices=TEMPAT_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
