from django.shortcuts import render, redirect
from .models import *
from .forms import *
from django.utils import timezone
from addwarung.models import *
from django.contrib.auth.decorators import login_required

# Create your views here.
def testi(request):
    if request.method == "POST":
        form = TestiForm(request.POST)
        if 'id' in request.POST:
            Testimoni.objects.get(id=request.POST['id']).delete()
            return redirect('/testimoni/')
        if form.is_valid():
            testi = Testimoni(
                waktu = timezone.localtime(timezone.now()).strftime("%Y-%m-%d %H:%M:%S"),
                user = request.user.username,
                warung = (Warung.objects.get(id=form.data['warung']).name),
                pesan = form.data['pesan']
                )
            testi.save()
            return redirect('/testimoni/')
    else:
        form = TestiForm()

    context = {
        'testimoni' : Testimoni.objects.all().values(),
        'form' : form,
    }
    return render(request, 'testi.html', context)

def listwarung(request):
    context = {
        'warung' : Warung.objects.all().values(),
        'barang' : Barang.objects.all().values(),
        }
    return render(request, 'listwarung.html', context)

def error404(request, exception=None):
    return render(request, '404.html')
