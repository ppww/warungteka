from django.urls import path
from testimoni import views

urlpatterns = [
    path('', views.testi, name='testimoni'),
]
