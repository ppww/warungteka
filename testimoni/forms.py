from django import forms
from testimoni import models

class TestiForm(forms.Form):
    warung = forms.ModelChoiceField(
        label = 'Warung',
        queryset = models.Warung.objects.all(),
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm'}),
    )
    pesan = forms.CharField(
        label = 'Pesan',
        required = True,
        widget = forms.Textarea(attrs = {'class' : 'form-control form-control-sm', 'style' : 'height: 120px;', 'placeholder' : 'Beri testimoni'}),
    )

    class Meta:
        model = models.Testimoni
        fields = ('warung', 'pesan',)