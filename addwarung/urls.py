from django.urls import path
from addwarung import views

urlpatterns = [
    path('', views.addwarung, name='addwarung'),
]
